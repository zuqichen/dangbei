package internal

type MovieItem struct {
	Title             string  `json:"title"`
	Description       string  `json:"desc"`
	CategoriesRaw     string  `json:"cat"`
	Aid               int64   `json:"aid"`
	Area              string  `json:"area"`
	Year              int     `json:"year"`
	Score             float64 `json:"score"`
	ActorsRaw         string  `json:"act"`
	DirectorRaw       string  `json:"director"`
	SevenDayPlayCount int     `json:"7day"`
	IsVip             string  `json:"is_vip"`
	Image             string  `json:"pic"`
}

type Movie struct {
	Title             string
	Description       string
	Aid               int64
	Area              string
	Year              int
	Score             float64
	SevenDayPlayCount int
	IsVip             bool
	Image             string
}

type Person struct {
	Name string
}

type Category struct {
	Name string
}
