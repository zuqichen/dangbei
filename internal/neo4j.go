package internal

import (
	"fmt"
	"log"

	"github.com/neo4j/neo4j-go-driver/v4/neo4j"
)

type Neo4jManager struct {
	driver  *neo4j.Driver
	session *neo4j.Session
}

func NewNeo4jManager(uri, username, password, database string) (*Neo4jManager, error) {
	driver, err := neo4j.NewDriver(uri, neo4j.BasicAuth(username, password, ""))
	if err != nil {
		return nil, fmt.Errorf("error when creating neo4j driver: %w", err)
	}

	session := driver.NewSession(neo4j.SessionConfig{AccessMode: neo4j.AccessModeWrite, DatabaseName: database})
	return &Neo4jManager{driver: &driver, session: &session}, nil
}

func (n *Neo4jManager) Destroy() {
	(*n.driver).Close()
	(*n.session).Close()
}

func (n *Neo4jManager) Write(query string, params map[string]interface{}) error {
	_, err := (*n.session).WriteTransaction(func(tx neo4j.Transaction) (interface{}, error) {
		result, err := tx.Run(query, params)
		if err != nil {
			return nil, fmt.Errorf("error when running neo4j query: %w", err)
		}
		if result.Next() {
			return result.Record().Values[0], nil
		}
		return nil, result.Err()
	})
	if err != nil {
		return err
	}
	return nil
}

func (p *Person) actIn(m *Movie, n *Neo4jManager) {
	fmt.Printf("%s acts in %s\n", p.Name, m.Title)
	// create actor (avoid duplication)
	err := n.Write(`
		MERGE (p:Person {name: $name})
		RETURN p
	`, map[string]interface{}{
		"name": p.Name,
	})
	if err != nil {
		log.Fatal(err)
	}
	// create movie (avoid duplication)
	err = n.Write(`
		MERGE (m:Movie {title: $title, description: $description, aid: $aid, area: $area, year: $year, score: $score, sevenDayPlayCount: $sevenDayPlayCount, isVip: $isVip, image: $image})
		RETURN m
	`, map[string]interface{}{
		"title":             m.Title,
		"description":       m.Description,
		"aid":               m.Aid,
		"area":              m.Area,
		"year":              m.Year,
		"score":             m.Score,
		"sevenDayPlayCount": m.SevenDayPlayCount,
		"isVip":             m.IsVip,
		"image":             m.Image,
	})
	if err != nil {
		log.Fatal(err)
	}
	// create relationship
	err = n.Write(`
		MATCH (p:Person {name: $name})
		MATCH (m:Movie {title: $title})
		MERGE (p)-[r:ACTED]->(m)
	`, map[string]interface{}{
		"name":  p.Name,
		"title": m.Title,
	})
	if err != nil {
		log.Fatal(err)
	}
}

func (p *Person) direct(m *Movie, n *Neo4jManager) {
	fmt.Printf("%s directs %s\n", p.Name, m.Title)
	// create director (avoid duplication)
	err := n.Write(`
		MERGE (p:Person {name: $name})
		RETURN p
	`, map[string]interface{}{
		"name": p.Name,
	})
	if err != nil {
		log.Fatal(err)
	}
	// create movie (avoid duplication)
	err = n.Write(`
		MERGE (m:Movie {title: $title})
		RETURN m
	`, map[string]interface{}{
		"title": m.Title,
	})
	if err != nil {
		log.Fatal(err)
	}
	// create relationship
	err = n.Write(`
		MATCH (p:Person {name: $name})
		MATCH (m:Movie {title: $title})
		MERGE (p)-[r:DIRECTED]->(m)
	`, map[string]interface{}{
		"name":  p.Name,
		"title": m.Title,
	})
	if err != nil {
		log.Fatal(err)
	}
}

func (m *Movie) belongTo(c *Category, n *Neo4jManager) {
	fmt.Printf("%s belongs to %s\n", m.Title, c.Name)
	// create movie (avoid duplication)
	err := n.Write(`
		MERGE (m:Movie {title: $title, aid: $aid})
		RETURN m
	`, map[string]interface{}{
		"title": m.Title,
		"aid":   m.Aid,
	})
	if err != nil {
		log.Fatal(err)
	}
	// create category (avoid duplication)
	err = n.Write(`
		MERGE (c:Category {name: $name})
		RETURN c
	`, map[string]interface{}{
		"name": c.Name,
	})
	if err != nil {
		log.Fatal(err)
	}

	// create relationship
	err = n.Write(`
		MATCH (m:Movie {title: $title})
		MATCH (c:Category {name: $name})
		MERGE (m)-[r:BELONG_TO]->(c)
	`, map[string]interface{}{
		"title": m.Title,
		"name":  c.Name,
	})
	if err != nil {
		log.Fatal(err)
	}
}
