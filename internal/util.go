package internal

import (
	"encoding/json"
	"io"
	"log"
	"os"
	"regexp"
	"strings"

	"github.com/joho/godotenv"
)

func ProduceData(r io.Reader, jobs chan *MovieItem) {
	dec := json.NewDecoder(r)
	for dec.More() {
		t, err := dec.Token()
		if err != nil {
			log.Fatalf("decoding tokens before [: %v", err)
		}
		if t == json.Delim('[') {
			for dec.More() {
				var item MovieItem
				err := dec.Decode(&item)
				if err != nil {
					log.Fatalf("decoding each movie item: %v", err)
				}
				jobs <- &item

			}
			break
		}
	}
	close(jobs)
}

func ConsumeData(ch chan *MovieItem, done chan bool) {
	godotenv.Load()
	uri, username, password, database := os.Getenv("NEO4J_URI"), os.Getenv("NEO4J_USERNAME"), os.Getenv("NEO4J_PASSWORD"), os.Getenv("NEO4J_DATABASE")
	manager, err := NewNeo4jManager(uri, username, password, database)
	defer manager.Destroy()
	if err != nil {
		log.Fatal(err)
	}
	for item := range ch {
		categories := processCategories(item)
		directors := processPeople(item.DirectorRaw)
		actors := processPeople(item.ActorsRaw)

		movie := &Movie{item.Title, item.Description, item.Aid, item.Area, item.Year, item.Score, item.SevenDayPlayCount, item.IsVip == "1", item.Image}
		for _, actor := range actors {
			actor.actIn(movie, manager)
		}

		for _, category := range categories {
			movie.belongTo(category, manager)
		}

		for _, director := range directors {
			director.direct(movie, manager)
		}
	}
	done <- true
}

func processCategories(item *MovieItem) []*Category {
	res := []*Category{}
	for _, catStr := range strings.Split(item.CategoriesRaw, ",") {
		if catStr == "" {
			continue
		}
		res = append(res, &Category{Name: catStr})
	}
	return res
}

func processPeople(rawStr string) []*Person {
	res := []*Person{}
	for _, pStr := range strings.Split(rawStr, ",") {
		// TODO: handle untranslated names
		re := regexp.MustCompile(`^[A-Za-z]+$`)
		if pStr == "" || re.MatchString(pStr) {
			continue
		}
		res = append(res, &Person{Name: pStr})
	}
	return res
}
