module dangbei

go 1.17

require (
	github.com/joho/godotenv v1.3.0
	github.com/neo4j/neo4j-go-driver/v4 v4.3.3
)
