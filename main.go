package main

import (
	"fmt"
	"log"
	"os"

	"dangbei/internal"
)

const (
	numOfConsumers = 20
)

func main() {
	fmt.Println("Reading the json data dump file...")
	if len(os.Args) != 2 {
		fmt.Printf("Usage: ./dangbei <PATH_TO_JSON_FILE>\n")
		return
	}
	filename := os.Args[1]
	f, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	jobs := make(chan *internal.MovieItem)
	done := make(chan bool)
	go internal.ProduceData(f, jobs)

	for i := 0; i < numOfConsumers; i++ {
		go internal.ConsumeData(jobs, done)
	}
	<-done
}
