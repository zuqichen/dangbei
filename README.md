# Dangbei movies data to Neo4j database

## Build

Create a `.env` file in the project root directory. The file may look like this:
```
NEO4J_URI=bolt://localhost:7687
NEO4J_USERNAME=neo4j
NEO4J_PASSWORD=neo4j
NEO4J_DATABASE=neo4j
```
After creating the `.env` file, run `go build`

## Run

After building, run `./dangbei <PATH_TO_JSON_FILE>`
OR
`go run main.go <PATH_TO_JSON_FILE>`

## Todos

- [ ] Handle untranslated actors / directors names (currently ignored)
- [ ] Encapsulate cypher query writing logic